#!/usr/bin/env bash

# Set all [array] to `value`
# Example: set_all arr "$value"
set-all() {
    name=$1[@]
    value=$2
    vars=("${!name}")

    for i in "${vars[@]}" ; do
      export $i="${value}"
    done
}

urlencode() {
  python -c 'import urllib, sys; print urllib.quote(sys.argv[1], sys.argv[2])' \
    "$1" "$urlencode_safe"
}
