#!/usr/bin/env bash

# to get user password, used in other .dotfiles
get-password() {
  echo "$(security find-internet-password -a ${USER} -s dstproxy.dstcorp.net -w)"
}
