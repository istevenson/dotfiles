#!/usr/bin/env bash

export OC_CLUSTER_SANDBOX=sandbox.cap.dstcorp.net
export OC_CLUSTER_DEV=dev.cap.dstcorp.net
export OC_CLUSTER_EAP=eap.cap.dstcorp.net
export OC_CLUSTER_PROD=prod.cap.dstcorp.net

# Construct OpenShift URL
oc-url() { echo https://openshift.$1:8443 }

# Reuse function to log in accepting $1=OpenShift URI
oc-login() {
  proxy_off
  oc login `oc-url $1` --username=${USER} --password=$(get_password)
  docker login -u $(oc whoami) -p $(oc whoami -t) docker-registry-default.$1
}

# Launch Browser for OpenShift UI
oc-ui() { open `oc-url $1` }

# Log in implementations
alias oc-sandbox='oc-login $OC_CLUSTER_SANDBOX'
alias oc-sandbox-ui='oc-ui $OC_CLUSTER_SANDBOX'
alias oc-dev='oc-login $OC_CLUSTER_DEV'
alias oc-dev-ui='oc-ui $OC_CLUSTER_DEV'
alias oc-eap='oc-login $OC_CLUSTER_EAP'
alias oc-eap-ui='oc-ui $OC_CLUSTER_EAP'
alias oc-prod='oc-login $OC_CLUSTER_PROD'
alias oc-prod-ui='oc-ui $OC_CLUSTER_PROD'
