#!/usr/bin/env bash

# Easy utilities for Git

# Open current directory project in browser
git-ui() {
  UI_URL=`git config --get remote.origin.url | sed "s/.*\@\(.*\):\(.*\)\..*/\1\/\2\/tree\//"`
  open http://$UI_URL`git branch | cut -d' ' -f 2`
}

# git-clone {repo} [base dir]
git-clone() {
  REPO=$1
  BASE_DIR=${2:-~}
  HTTPS=false

  if `echo $REPO | sed -n "/^http:/p"`
  then
    NS=`echo $REPO | sed "s/https:\/\/\(.*\)\/\(.*\)\..*/\1/"`
    HTTPS=true
  else
    NS=`echo $REPO | sed "s/.*\@\(.*\):\(.*\)\/\(.*\)\..*/\1\/\2/"`
  fi

  mkdir -p $BASE_DIR/$NS
  cd $BASE_DIR/$NS
  git clone $REPO
  if $HTTPS ; then
    cd `echo $REPO | sed "s/https:\/\/\(.*\)\/\(.*\)\..*/\2/"`
  else
    cd `echo $REPO | sed "s/.*\@\(.*\)\..*:\(.*\)\/\(.*\)\..*/\3/"`
  fi
}

git-push() {
  MSG=${1:-"Quick Push"}
  git add . && git commit -m $MSG && git pull && git push
}

git-update() {
  for REPO in `find . -name '.git' -type d`
  do
    DIR=`echo $REPO | sed "s/\(.*\)\.git/\1/"`
    (cd $DIR >> /dev/null && echo "Updating `pwd`" && git stash && git pull && git stash pop && echo "")
  done
}