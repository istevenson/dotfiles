#!/bin/bash

alias go-d='go run *.go'

go-clone() {
    git-clone $1 $GOPATH/src
}