#!/usr/bin/env bash

export DOTFILE_DIR=`dirname $0`

source-all() {
  for DOTFILE in `find "$1"/*.sh`
  do
    [ -f $DOTFILE ] && source $DOTFILE
  done
}

source $DOTFILE_DIR/manifest.sh
