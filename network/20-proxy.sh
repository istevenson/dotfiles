#!/usr/bin/env bash

export PROXY_ADDR="dstproxy.dstcorp.net:9119"

get-no-proxy-list() {
  networksetup -getproxybypassdomains "`get-current-network-service`" |
  while read interface; do
    local no_proxies="$no_proxies, $interface"
  done
  echo $no_proxies | cut -c 3-
}

get-proxy() {
  echo "http://${USER}:$(urlencode `get-password`)@${PROXY_ADDR}"
}

# Proxy managment functions
proxy-on() {
    export http_proxy="`get-proxy`"
    export https_proxy="${http_proxy}"
    export no_proxy="`get-no-proxy-list`"
    export HTTP_PROXY="${http_proxy}"
    export HTTPS_PROXY="${https_proxy}"
    export NO_PROXY="${no_proxy}"
    proxy-status
}

proxy-off() {
    unset http_proxy https_proxy no_proxy HTTP_PROXY HTTPS_PROXY NO_PROXY
    proxy-status
}

proxy-status() {
    if [ -z ${http_proxy+x} ]; then
        printf "DST proxy is disabled.\n"
    else
        printf "DST proxy is enabled.\n"
    fi
}
